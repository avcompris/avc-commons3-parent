# About avc-commons3-parent

This is a new flavor
of [avc-commons-parent](https://gitlab.com/avcompris/avc-commons-parent/), a base POM
for Avantage Compris project.

Projects that inherit from this one include:

  * [avc-commons3-types](https://gitlab.com/avcompris/avc-commons3-types/)
  * [avc-commons3-yaml](https://gitlab.com/avcompris/avc-commons3-yaml/)
  * [avc-commons3-databeans](https://gitlab.com/avcompris/avc-commons3-databeans/)


This is the project home page, hosted on GitLab.

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-commons3-parent/)

